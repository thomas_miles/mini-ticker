extern crate ws;
extern crate serde_json;

use ws::{connect, Handler, Sender, Handshake, Result, Message, CloseCode, Error};
use evaluator::Evaluator;


pub struct BitstampTicker {
    streaming_url: String
}

impl BitstampTicker {

    pub fn new() -> Self {
        BitstampTicker { 
            streaming_url: "ws://ws.pusherapp.com:80/app/de504dc5763aeef9ff52?client=rust-minitick&version=0.1&protocol=7".to_owned()
        } 
    }

    pub fn connect_and_stream(&mut self) {
        // Connect via web socket and stream/evaluate data
        let url = self.streaming_url.as_mut_str();
        connect(
            url, |out|
            Client {
                out: out, 
                eval: Evaluator::new()
        })
        .unwrap()
    }
}

#[derive(Deserialize)]
struct Tick {
    event: String,
    data: String
}

struct Client {
    out: Sender,
    eval: Evaluator
}

impl Client {

    fn handle (&mut self, message: String) {
        let msg: Tick = self.deserialize_ticker_message(message);
        match msg.event.as_str() {
            "order_created" => self.eval.eval_order(serde_json::from_str(&msg.data).unwrap()),
                          _ => ()
        }
    }

    fn deserialize_ticker_message(&mut self, message: String) -> Tick {
        serde_json::from_str(&message).unwrap()
    }

}

impl Handler for Client {

    fn on_open(&mut self, _shake: Handshake) -> Result<()> {
        let subscribe = json!({
	        "event": "pusher:subscribe",
	        "data": {
		        "channel": "live_orders"
	        }
        });
        self.out.send(subscribe.to_string())
    }
 
    fn on_message(&mut self, msg: Message) -> Result<()> {
        let message = msg.into_text();
        match message {
            Ok(m)  => self.handle(m),
            Err(e) => warn!("Error =  {}", e),
        }
        Ok(())
    }

    fn on_close(&mut self, _code: CloseCode, reason: &str) {
        info!("Connection closed = {}", reason);
    }

    fn on_error(&mut self, err: Error) {
        warn!("Error = {}", err);
    }

}
