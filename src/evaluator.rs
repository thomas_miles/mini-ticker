
#[derive(Deserialize)]
pub struct Order {
    price: f32,
    amount: f32,
    order_type: i32
}

pub struct Evaluator {
    historic_buy_orders: Vec<Order>,
    historic_sell_orders: Vec<Order>
}

impl Evaluator {

    pub fn new() -> Self {
        Evaluator {
                historic_buy_orders: Vec::with_capacity(100),
                historic_sell_orders: Vec::with_capacity(100)
            } 
    }

    pub fn eval_order(&mut self, order: Order) {
        match order.order_type {
            0 => self.eval_buy_order(order),
            1 => self.eval_sell_order(order),
            _ => ()
        }
    }

    fn eval_buy_order(&mut self, order: Order) {
        info!("BUY  - price: {}, amount: {}", order.price, order.amount);
        if !self.last_buy_orders_are_lower_than_moving_average(10) {
            // ToDo: create sell order logic
        }
        self.add_historic_buy_order(order);
        
    }

    fn eval_sell_order(&mut self, order: Order) {
        info!("SELL - price: {}, amount: {}", order.price, order.amount);
        // If the last sell orders are on an upward trend, create a buy order if the 
        // previous sell order is more than 3% lower.
        if !self.last_sell_orders_are_lower_than_moving_average(10) {
            if self.sell_order_price_compared_to_previous_is_lower_than_delta(order.price, 3) {
                // ToDo: create buy order
                info!("Creating buy order for {}", order.price);
            }
        } 
        self.add_historic_sell_order(order);
    }

    fn add_historic_buy_order(&mut self, order: Order) {
        // Basic stack implementation for collecting the previous 100 buy orders
        if self.historic_buy_orders.len() >= self.historic_buy_orders.capacity() {
            self.historic_buy_orders.pop();
        }
        self.historic_buy_orders.insert(0, order);
    }

    fn add_historic_sell_order(&mut self, order: Order) {
        // Basic stack implementation for collecting the previous 100 sell orders
        if self.historic_sell_orders.len() >= self.historic_sell_orders.capacity() {
            self.historic_sell_orders.pop();
        }
        self.historic_sell_orders.insert(0, order);
    }

    fn get_buy_order_moving_average(&mut self) -> f32 {
        // Returns the average price of the previous 100 buy orders
        let iter = self.historic_buy_orders.iter();
        let total: f32 = iter.map(|order| order.price)
            .sum();
        total / self.historic_buy_orders.len() as f32
    }

    fn get_sell_order_moving_average(&mut self) -> f32 {
        // Returns the average price of the previous 100 sell orders
        let iter = self.historic_sell_orders.iter();
        let total: f32 = iter.map(|order| order.price)
            .sum();
        total / self.historic_sell_orders.len() as f32
    }

    fn last_buy_orders_are_lower_than_moving_average(&mut self, count: i32) -> bool {
        // Returns true if the average of the last X buy orders is lower than 
        // the average of the last 100 buy orders
        if count > self.historic_buy_orders.len() as i32 {
            return false
        }
        let average = self.get_buy_order_moving_average();
        let last_buy_orders: f32 = self.historic_buy_orders.iter()
            .rev()
            .take(count as usize)
            .map(|order| order.price)
            .sum();
        if last_buy_orders < average {
            return true
        }
        false 
    }

    fn last_sell_orders_are_lower_than_moving_average(&mut self, count: i32) -> bool {
        // Returns true if the average of the last X sell orders is lower than 
        // the average of the last 100 sell orders
        if count > self.historic_sell_orders.len() as i32 {
            return false
        }
        let average = self.get_sell_order_moving_average();
        let last_sell_orders: f32 = self.historic_sell_orders.iter()
            .rev()
            .take(count as usize)
            .map(|order| order.price)
            .sum();
        if last_sell_orders < average {
            return true
        }
        false
    }

    fn sell_order_price_compared_to_previous_is_lower_than_delta(&mut self, price: f32, delta: i32) -> bool {
        // Returns true if the price of the order is lower than the price of the
        // previous order * delta percentage
        let previous_order = self.historic_sell_orders.last();
        if previous_order.is_none() {
            return false
        }
        let previous_order_price = previous_order.unwrap().price;
        if (previous_order_price * ((100 - delta) / 100) as f32) < price {
            return false
        }
        true
    }
}
