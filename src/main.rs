mod ticker;
mod evaluator;

extern crate ws;
extern crate serde;
extern crate simple_logger;

#[macro_use]
extern crate serde_json;

#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate log;

use log::LogLevel;
use ticker::BitstampTicker;


fn main() {

    simple_logger::init_with_level(LogLevel::Info).unwrap();
    BitstampTicker::new().connect_and_stream();

}
